#include <iostream>

int main()
{
    int const N = 3;
    int data = 9;
    int Matrix[N][N];
    int summ = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            Matrix[i][j] = i + j;
            std::cout << Matrix[i][j];
            if (i == data % N)
            {
                summ = summ + Matrix[i][j];
            }
        }
        std::cout << "\n";
    }
    std::cout << summ << "\n";
}
